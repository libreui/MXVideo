package com.mx.video.beans

import java.io.Serializable


/**
 * 播放屏幕类型枚举
 */
enum class MXScreen : Serializable {
    FULL, // 全屏
    NORMAL  // 小屏
}